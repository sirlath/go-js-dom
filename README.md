## Fork 

This is a fork of https://github.com/dominikh/go-js-dom. It was forked due to the lack of go mod support for version 1
of the lib. Support for go mod exist in version 2, but it's currently marked as unable and alpha so in the meantime this fills the gap. 

# js/dom

Package dom provides Go bindings for the JavaScript DOM APIs.


## Version 1

**API Status:** Stable, changes only due to DOM being a moving target

Version 1 of package dom is implemented on top of the [`github.com/gopherjs/gopherjs/js`](https://godoc.org/github.com/gopherjs/gopherjs/js) API and supports GopherJS only.

### Install

    go get gitlab.com/sirlath/go-js-dom

### Documentation

For documentation, see https://godoc.org/honnef.co/go/js/dom.
